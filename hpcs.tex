
\section{Data Collection}
\label{sec:hpcs}

In this section, we outline how HPCs have been collected and processed to build
and verify power models for the \tegra. All profiler code and kernel patches can be downloaded via the links
found at \url{folk.uio.no/krisrst/}. 

\begin{figure}
    \centering
    \includegraphics[scale=0.2]{figures/tk1hw}
    \caption{Overview of the \tegra hardware platform.}
    \label{fig:tk1hw}
\end{figure}

\subsection{Power Usage of CMOS Devices}

In order to understand how the dataset provided in this paper is structured, it
is necessary to provide a brief background of power management on the \jetson
and how CMOS devices consume power. In general, the power consumption of CMOS devices can be
described as the sum of static and dynamic power~\cite{Kim2003}:

\begin{equation}
    P_{device} = P_{static} + P_{dynamic}
    \label{equ:cmos}
\end{equation}

In Equation~\ref{equ:cmos}, dynamic and static power depends on operating
frequency $f$ and supply voltage $V$ as follows:

\begin{equation}
    P_{dynamic} = \alpha C V^2 f
    \label{equ:dyn}
\end{equation}
\begin{equation}
    P_{static} = I_{leak} V
    \label{equ:stat}
\end{equation}

In Equations~\ref{equ:dyn} and \ref{equ:stat}, $C$ is the maximum possible capacitive load (in
coloumbs per volt) per switching cycle, $\alpha \in [0,1]$ is an
\textit{activity factor} describing the average switching activity of the
circuitry over time, and $I_{leak}$ is a constant leakage current (in amperes).
$I_{leak}$ has dependencies on chip temperature, however, in our dataset the
device is actively cooled at room temperature using the default cooling fan of
the \jetson. The fan is externally powered and is not a part of the power
measurements.

\begin{figure}
    \begin{center}
        \begin{subfigure}[t]{0.3\textwidth}
            \includegraphics[scale=0.05, trim = 9cm 0cm 0cm 0cm, clip = true]{lpvol.pdf}
            \caption{LP and memory.}
            \label{fig:lpvol}
        \end{subfigure}
        \begin{subfigure}[t]{0.3\textwidth}
            \includegraphics[scale=0.05, trim = 2cm 0cm 0cm 0cm, clip = true]{hpvol}
            \caption{HP.}
            \label{fig:hpvol}
        \end{subfigure}
        \begin{subfigure}[t]{0.3\textwidth}
            \includegraphics[scale=0.05, trim = 2cm 0cm 0cm 0cm, clip = true]{gpuvol.pdf}
            \caption{GPU.}
            \label{fig:gpuvol}
        \end{subfigure}

        \begin{subfigure}[t]{0.3\textwidth}
            \includegraphics[scale=0.05, trim = 9cm 0cm 0cm 0cm, clip = true]{lppow}
            \caption{LP and memory.}
            \label{fig:lppow}
        \end{subfigure}
        \begin{subfigure}[t]{0.3\textwidth}
            \includegraphics[scale=0.05, trim = 9cm 0cm 0cm 0cm, clip = true]{hppow}
            \caption{HP and memory.}
            \label{fig:hppow}
        \end{subfigure}
        \begin{subfigure}[t]{0.3\textwidth}
            \includegraphics[scale=0.05, trim = 9cm 0cm 0cm 0cm, clip = true]{gpupow.pdf}
            \caption{GPU and memory.}
            \label{fig:gpupow}
        \end{subfigure}
    \end{center}
    \caption{\label{fig:cmosmrs}Measured voltage (top row) and power (bottom row) under various frequency ranges.}
\end{figure}


The \tegra is a heterogeneous multicore processor, providing developers access
to a low-power CPU core, a high-performance CPU quad-core cluster, and
a Kepler-based, 192-core Nvidia GPU~\cite{NVIDIA}. These computational units,
and RAM, are powered by \textit{rails} with regulators designed to downconvert supply voltage to
the components (see Figure~\ref{fig:tk1hw}). The required supply voltage depends
on operating frequency. For example, in Figures~\ref{fig:lpvol}, \ref{fig:hpvol}
and \ref{fig:gpuvol}~\cite{StokkePhD}, we see that the supply voltage of the
LP core, HP cluster and GPU increases with the operating frequency of these
units, as measured by a Keithley~2110 multimeter. The required
rail voltage is always the maximum required by any clock beloning to the circuit
domains supplied by that rail. The LP core rail, for example, has two dependencies, the LP
core clock and the memory controller clock. For the HP rail, only the CPU
cluster clock is important, where rail voltage starts increasing when CPU clock
frequency increases beyong 1.3~GHz. This is similar to the GPU rail, where
voltage remains at 0.79~V until GPU clock frequency increases beyond 400~MHz.
The relation between voltage, clock frequency and power (see
Equations~\ref{equ:dyn} and \ref{equ:stat}) can be verified if we measure the total platform
power and vary the rail voltage and domain frequency in Figures~\ref{fig:lppow},
\ref{fig:hppow} and \ref{fig:gpupow}. Studying these figures, we can see
a linear increase in power as frequency is increased ($P_{dyn} \propto f$),
and a superlinear increase in power as voltage starts climbing ($P_{dyn} \propto
V^2$). Note that the memory rail voltage is statically set to 1.35~V and
does not vary with memory clock frequency, however, the memory controller clock
does affect the LP core rail voltage.

\subsection{Power and Voltage Measurements}

Power measurement is the subject of many research papers, with a wide range of
solutions. Some are based on using existing~\cite{Carroll2010} or manually
inserting~\cite{Rice2010} sense resistors in series with the components under
study, sampling the voltage drop across the resistor and rail voltage to
calculate instantaneous power draw using Ohm's law. The use of existing
sensors has in several cases been shown to suffer from poor
accuracy~\cite{Dong2011,Jung2012}. Other researchers resort to external power
measurement setups~\cite{Pathak2011,Limin2007,Xiao2010,Vatjus2013} where the
measurement unit also supplised power to the device under study. 

\begin{figure}
    \centering
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{k2280ssetup}
        \caption{Measurement setup.}
        \label{fig:k2280ssetup}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{psync}
        \caption{Measurement synchronisation.}
        \label{fig:psync}
    \end{subfigure}
    \caption{\label{fig:k2280s}Power measurement setup using the Keithley K2280S
    SMU.}
\end{figure}

The \jetson does not incorporate any measurement sensors, and therefore a custom
solution is needed. We use a Keithley K2280S SMU which both powers and measures
current draw with an error of only 0.02 \% and a resolution between $10 nA$ and
$10 \mu A$. We configure the SMU to log samples at 1~kHz, and use an external
machine  (logger in Figure~\ref{fig:k2280ssetup}) to administer the SMU and log
samples. The \tegra starts, stops and downloads measurement samples from the
logger as required. 

Due to the short sample interval (1~ms) the samples' timestamps must be
synchronised with those of the \tegra~\cite{Rice2010}. The \tegra therefore cycles the CPU
frequency from minimum to maximum in a fixed interval. This signature is visible
in Figure~\ref{fig:psync} as changes in power draw. After downloading
measurement samples, the \tegra scans the power traces for this signature,
determining and changing the time shift of the samples to match local time.

For power modelling purposes, rail voltage is an important factor that must also
be logged (see Equations~\ref{equ:dyn} and \ref{equ:stat}). However, there are no on-board
voltage measurement sensors on the \jetson. Therefore, we use a Keithley K2110
multimeter to measure rail voltages at different operating frequencies, creating
static frequency-voltage tables in our profiler. 

\subsection{Measuring Hardware Activity}

Our dataset is originally intended for power modelling, necessitating the need
for model predictors that capture hardware (switching)
activity~\cite{Pricopi2013,Xiao2010}. The choice of predictors is designed to reflect
hardware utilisation as generically as possible for a wide range of workloads.
In most cases these predictors are HPCs normally used to analyse the performance
of software through various libraries. All HPCs are counted in minimum 100~ms
intervals. For the CPU, PERF, a Linux kernel
interface used to collect HPCs from CPU cores such as the \tegra's ARM
Cortex-A15 cores~\cite{A15TRM}, is used to collect the following HPCs (per CPU
core):
\begin{itemize}
    \item Active processor cycles.
    \item Total number of executed instructions.
    \item L1 data cache refills.
    \item L1 instruction cache refills.
    \item L1 TLB cache refills.
    \item L2 cache refills.
    \item Time spent in power-gated state.
\end{itemize}

\begin{figure}
    \centering
    \includegraphics[scale=0.35]{expdesign}
    \caption{HPC monitoring.}
    \label{fig:expdesign}
\end{figure}

For the \tegra's GPU, HPC collection is more complex. HPC collection is done
through the CUPTI (CUDA Profiling Tools Interface) library (see
Figure~\ref{fig:expdesign}) through \textit{callbacks} that trigger on CUDA kernel
launches and exits. In these callbacks, measurement of groups of HPCs must be triggered prior
to the launch, and logged on exits. Because the measurement of HPCs incurs
additional runtime overhead (slows down kernels) it is not feasible to have them
running continuously. Instead, at any kernel launch, a hash is computed for that
kernel. After all groups of HPCs are collected for that kernel, the HPCs are
simply logged on kernel exit as normal. The HPCs collected using this method
are:
\begin{itemize}
    \item Instructions: Integer, single-precision floating point,
        double-precision floating point, bit conversion, control and
        miscellaneous.
    \item L2 cache reads and writes.
    \item L1 cache local store and load hits.
    \item L1 cache global load hit.
    \item L1 cache shared loads and stores.
\end{itemize}

Finally, to monitor RAM activity, we use an \textit{activity monitor} present in
the \tegra SoC to monitor the number of active memory controller cycles spent
serving CPU or other (GPU) hardware units. The activity monitor is originally
intended to guide RAM frequency scaling algorithms, and the counters are not
exposed to userspace applications. Therefore, this requires changes to the Linux
kernel to expose these counters to applications through sysfs. 

