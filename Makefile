filename := output
texfiles := $(wildcard *.tex)

$(filename).pdf: $(texfiles) all.bib $(qcmpplots) $(bwplots) $(cdfplots) $(stabplots) $(miscplots)
	pdflatex paper
	bibtex paper
	pdflatex paper
	pdflatex paper

output:
	cp paper.pdf $@

clean:
	rm -f paper.pdf paper.log paper.blg paper.aux paper.bbl $(filename).pdf *~

